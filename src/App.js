import React, { Fragment, lazy, Suspense } from 'react'
import { Helmet } from 'react-helmet'
import { useQuery } from 'react-query'
import { graphQlToRest } from './Data/fetchData'
import CategoryHeader from './Components/CategoryHeader/CategoryHeader'
import CategoryButton from './Components/CategoryButton/CategoryButton'
import Loading from './Components/Loading/Loading'
import './app.scss'

const SmallCard = lazy(() => import('./Components/Cards/SmallCard/SmallCard'))
const BigCard = lazy(() => import('./Components/Cards/BigCard/BigCard'))
const GridCard = lazy(() => import('./Components/Cards/GridCard/GridCard'))

function App() {
	const { isLoading, data } = useQuery('fetch_data', graphQlToRest, {
		select: (item) => item.data.getHomePageArticle.aResults,
	})

	if (isLoading) {
		return <Loading />
	}

	return (
		<Suspense fallback={<Loading />}>
			<div className='app'>
				{data?.map((item, i) => {
					return (
						<Fragment key={i}>
							<CategoryHeader name={item.sName} key={i} />
							{item.aArticle.map((Card) => (
								<Fragment key={Card._id}>
									{Card.sType == 'nBig' && <BigCard data={Card} />}
									{Card.sType == 'nSmall' && <SmallCard data={Card} />}
								</Fragment>
							))}

							<div className='grid_card_container'>
								{item.aArticle.map(
									(gCard) =>
										gCard.sType == 'nGrid' && (
											<GridCard key={gCard._id} data={gCard} />
										)
								)}
							</div>
							<CategoryButton name={item.sName} />
						</Fragment>
					)
				})}
				<Helmet>
					<title> welcome to Crictracker</title>
				</Helmet>
			</div>
		</Suspense>
	)
}

export default App
