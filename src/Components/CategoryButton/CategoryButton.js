import React from 'react'
import PropTypes from 'prop-types'
import './categoryButton.scss'

function CategoryButton({ name }) {
	return (
		<button className='category_button'>
			More from {name.slice(0, 30)} {' >'}
		</button>
	)
}

CategoryButton.propTypes = {
	name: PropTypes.string,
}
export default CategoryButton
