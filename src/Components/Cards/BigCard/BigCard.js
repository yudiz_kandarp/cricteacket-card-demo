import React from 'react'
import PropTypes from 'prop-types'
import ClockIcon from '../../../Assets/clock_icon.svg'
import { format } from 'date-fns'
import './bigCard.scss'

function BigCard({ data }) {
	const date = format(new Date(data.dPublishDate), 'd MMM yyyy')

	return (
		<div className='big_card'>
			<img
				className='hero_img'
				src={
					data?.oImg.sUrl ||
					'https://resources.pulse.icc-cricket.com/photo-resources/2022/04/06/86ee652c-2591-4c0a-9889-dab75db39e4d/MS-Dhoni-versus-New-Zealand-2019.jpg?width=440&height=248'
				}
				alt=''
				onError={(e) => {
					e.target.src =
						'https://resources.pulse.icc-cricket.com/photo-resources/2022/04/06/86ee652c-2591-4c0a-9889-dab75db39e4d/MS-Dhoni-versus-New-Zealand-2019.jpg?width=440&height=248'
				}}
			/>
			<div className='bCard_body'>
				<a href='#'>{data?.sTitle}</a>
				<p className='bCard_desc'>{data?.sDescription}</p>
				<p>
					<img
						src={
							'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSczBexy6V9ZX3mKGUWt5l_PgnXBB_8Afys8Q&usqp=CAU'
						}
						alt=''
					/>{' '}
					{date}
					<img src={ClockIcon} alt='' />
					{data.nDuration + ' min'}
				</p>
			</div>
		</div>
	)
}
BigCard.propTypes = {
	data: PropTypes.object,
}
export default BigCard
