import React from 'react'
import PropTypes from 'prop-types'
import './categoryHeader.scss'

function CategoryHeader({ name }) {
	return (
		<div className='category_header'>
			<div className='divider'></div>

			<div className='category_name_container'>
				<div className='category_name'>{name.slice(0, 30)} </div>
			</div>
		</div>
	)
}

CategoryHeader.propTypes = {
	name: PropTypes.string,
}
export default CategoryHeader
