import React from 'react'
import './loading.scss'

function Loading() {
	return (
		<div className='loading'>
			<div className='loader'></div>
		</div>
	)
}

export default Loading
