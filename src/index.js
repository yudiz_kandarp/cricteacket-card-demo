import React from 'react'
import ReactDOM from 'react-dom/client'
import * as Sentry from '@sentry/react'
import { BrowserTracing } from '@sentry/tracing'
import { QueryClientProvider, QueryClient } from 'react-query'
import App from './App'
import './index.scss'

Sentry.init({
	dsn: 'https://1c8fb0ee6bd0422888c8abdc74be6996@o1203707.ingest.sentry.io/6330275',
	integrations: [new BrowserTracing()],
	tracesSampleRate: 1.0,
})

const queryClient = new QueryClient()

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
	<QueryClientProvider client={queryClient}>
		<App />
	</QueryClientProvider>
)
